# Monokai Colorscheme for Vim

This is yet another Vim port of the TextMate's Monokai Colorscheme.

## Configuration

Just set the colorscheme in your `vimrc` file:

```
colorscheme monokai
```
